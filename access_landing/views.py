# -*- coding: utf-8 -*-
# Copyright (c) 2019 Marco Marinello <me@marcomarinello.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.views.generic import TemplateView
from . import models


class Home(TemplateView):
    template_name = "index.html"

    def get_context_data(self, *a, **kw):
        ctx = super().get_context_data(*a, **kw)
        ctx["active"] = models.Institute.objects.filter(active=True)
        ctx["inactive"] = models.Institute.objects.filter(active=False)
        return ctx
