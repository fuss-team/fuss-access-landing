# -*- coding: utf-8 -*-
# Copyright (c) 2019 Marco Marinello <me@marcomarinello.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Institute(models.Model):
    name = models.CharField(max_length=1000, verbose_name=_("institute name"))

    city = models.CharField(max_length=1000, verbose_name=_("city"))

    internal_host = models.CharField(
        max_length=1000,
        verbose_name=_("internal host"),
        help_text=_("host which the proxy should point to (with protocol)"),
        null=True,
        blank=True
    )

    external_host = models.CharField(
        max_length=1000,
        verbose_name=_("external host"),
        help_text=_("proxy fqdn (without protocol)"),
        null=True,
        blank=True
    )

    active = models.BooleanField(default=True, verbose_name=_("active"))

    def __str__(self):
        return f"{self.name} ({self.external_host} -> {self.internal_host})"


    class Meta:
        verbose_name = _("Institute")
        verbose_name_plural = _("Institutes")
        ordering = ("name", )
